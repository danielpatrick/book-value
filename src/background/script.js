const searchUrls = {
  Wordery: ({ isbn13 }) => `https://wordery.com/search?term=${isbn13}`,
  Hive: ({ isbn13 }) => `https://www.hive.co.uk/Search/Keyword?keyword=${isbn13.replace("-", "")}&productType=1`,
};

const getBookUrl = async ({ sellerName, isbns }) => {
  const { [sellerName]: urlCreator } = searchUrls;

  if (!urlCreator) {
    return null;
  }

  const requestUrl = urlCreator(isbns);
  const method = "HEAD";

  const { url } = await fetch(requestUrl, { method });

  return url;
}

const openUrlInNewTab = async ({ url }, sender) => {
  // index = undefined is better than NaN (tested in FF only)
  const index = (sender?.tab?.index + 1) || undefined;
  const active = false;

  await browser.tabs.create({ url, index, active });
}

const dispatch = {
  openUrlInNewTab,
  getBookUrl,
};

browser.runtime.onMessage.addListener(
  async ({ command, ...options }, sender) => {
    return await dispatch[command](options, sender);
  }
);
