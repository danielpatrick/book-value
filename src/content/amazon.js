const sendCommand = async (command, options) => {
  return await browser.runtime.sendMessage({ command, ...options });
};

const findISBNs = () => {
  let isbn10 = null;
  let isbn13 = null;

  const elems = document.getElementsByClassName("a-section rpi-attribute-content");
  for (const elem of elems) {
    if (elem.firstElementChild?.firstElementChild?.innerHTML?.includes("ISBN-10")) {
      isbn10 = elem.lastElementChild?.firstElementChild?.innerHTML;
    }
    if (elem.firstElementChild?.firstElementChild?.innerHTML?.includes("ISBN-13")) {
      isbn13 = elem.children[2]?.firstElementChild?.innerHTML;
    }
  }

  return {
    isbn10,
    isbn13,
  };
};

const createButton = sellerName => {
  return `
  <div class="celwidget book-value">
    <div class="a-button-stack">
      <div class="a-section a-spacing-base">
        <span id="buy-from-${sellerName.toLowerCase()}-span" class="a-button a-button-oneclick a-button-disabled a-button-icon onml-buy-now-button buybox-button-enhancement-size">
          <span class="a-button-inner">
            <i class="a-icon a-icon-cart"></i>
            <input id="buy-from-${sellerName.toLowerCase()}-button" class="a-button-input" title="Buy from ${sellerName}" value="Buy from ${sellerName} type="submit" disabled>
            <span class="a-button-text" aria-hidden="true"> Buy from ${sellerName}</span>
          </span>
        </span>
      </div>
    </div>
  </div>`;
};

const createButtonsDiv = sellerNames => {
  const divId = "book-value-container";

  if (document.getElementById(divId)) {
    return;
  }

  const container = document.getElementById("buybox")?.getElementsByClassName("a-box-inner")?.[0];
  const firstChild = container.firstChild;

  const div = document.createElement("div");
  div.classList = "a-accordion-inner accordion-row-content book-value";
  div.id = divId;

  div.innerHTML = `
  <div class="a-section">
    ${sellerNames.map(name => createButton(name)).join("")}
  </div>`;

  container.insertBefore(div, firstChild);
};

const injectLink = async (urlPromise, sellerName) => {
  const url = await urlPromise;
  if (!url) {
    return;
  }

  const btn = document.getElementById(`buy-from-${sellerName}-button`);
  btn.addEventListener("click", async e => {
    e.preventDefault();
    await sendCommand("openUrlInNewTab", { url });
  });
  btn.disabled = false;
  document.getElementById(`buy-from-${sellerName}-span`).classList.remove("a-button-disabled");
};

const injectLinks = async (sellerNames, isbns) => {
  await Promise.all(
    sellerNames.map(
      sellerName => injectLink(
        sendCommand("getBookUrl", { sellerName, isbns }),
        sellerName.toLowerCase(),
      )
    )
  );
};

// Actually start doing something...

console.log("Loading Book Value...");

const sellerNames = ["Wordery", "Hive"];
const isbns = findISBNs();

if (Object.values(isbns).some(val => !!val)) {
  createButtonsDiv(sellerNames);

  injectLinks(sellerNames, isbns);
}

console.log("Book Value loaded");
