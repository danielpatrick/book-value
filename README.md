# Book Value

Firefox add-on for comparing the price of books

## Installing

Releases can be found [here](https://gitlab.com/danielpatrick/book-value/-/releases).

To install, download the package (XPI) for the latest release and open with Firefox.
